#include "pessoa.hpp"
#include <string>

using namespace std;

Pessoa::Pessoa(){
        nome = "";
        idade = "";
        telefone = "";
}

Pessoa::Pessoa(string nome, string idade, string telefone){
	setNome(nome);
	setIdade(idade);
	setTelefone(telefone);
}

string  Pessoa::getNome() {
        return nome;
}

void Pessoa::setNome(string nome) {
        this->nome = nome;
}

string  Pessoa::getIdade() {
        return idade;
}

void Pessoa::setIdade(string idade) {
        this->idade = idade;
}

string  Pessoa::getTelefone() {
        return telefone;
}

void Pessoa::setTelefone(string telefone) {
        this->telefone = telefone;
} 
class Aluno: public Pessoa
{
protected: 
        int matricula;
public:
       pessoa()
       {
         matricula = 0;
       }
       int getMatricula()
       { 
         return matricula;
       }
       void setMatricula(int matricula)
       { 
         this-> matricula= matricula;
       }
};
class Professor: public Pessoa
{
protected: 
        int matricula;
        char  email;
public: 
      pessoa()
      int getMatricula()
      {
        return matricula;
      }
      int getEmail()
      { 
        return email;
      }
      void setMatricula(int matricula)
      {
        this -> matricula = matricula;
      }
      void setEmail(char email)
      {
        this -> email= email;
      }
 };  

